(local configs (require :nvim-treesitter.configs))
(local parsers (require :nvim-treesitter.parsers))

(configs.setup {
	:ensure_installed [
		:c
		:lua
		:vim
		:vimdoc
		:query
		:haskell
		:go
		:bash
		:python
		:fennel
		:hyprlang
	]

	:sync_install false
	:auto_install true

	:highlight {
		:enable true
	}
})

(vim.filetype.add {
	:pattern { :.*/hypr/.*%.conf :hyprlang }})
