(import-macros {: map!} :hibiscus.vim)

(local harpoon (require :harpoon))
(harpoon.setup)

(map! [n :noremap] :<leader>ha (fn [] (harpoon.list:append)))

(map! [n :noremap] :<C-e> (fn [] (harpoon.ui.toggle_quick_menu (harpoon.list))))

(map! [n :noremap] :<C-h> (fn [] (harpoon.list:select 1)))
(map! [n :noremap] :<C-t> (fn [] (harpoon.list:select 2)))
(map! [n :noremap] :<C-n> (fn [] (harpoon.list:select 3)))
(map! [n :noremap] :<C-s> (fn [] (harpoon.list:select 4)))

;; Toggle previous & next buffers stored within Harpoon list
(map! [n :noremap] :<C-S-P> (fn [] (harpoon.list:prev)))
(map! [n :noremap] :<C-S-N> (fn [] (harpoon.list:next)))

