(local plug (require :nvim-web-devicons))

(plug.setup [
	:strict true
	:default true
	:color_icons true

	:override [
		:zsh [
			:icon ""
			:color "#428850"
			:cterm_color "65"
			:name "zsh"
		]
	]

	:override_by_filename [
		:.gitignore [
			:icon ""
			:color "#f1502f"
			:name "gitignore"
		]
	]

	:override_by_extension [
		:log [
			:icon ""
			:color "#81e043"
			:name "Log"
		]
	]

])
