(local cursorword (require :mini.cursorword))
(cursorword.setup)

(local trailspace (require :mini.trailspace))
(trailspace.setup)

(local pairs (require :mini.pairs))
(pairs.setup)

(local clue (require :mini.clue))
(clue.setup)

(local surround (require :mini.surround))
(surround.setup)
