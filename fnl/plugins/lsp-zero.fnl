(local mason (require :mason))
(mason.setup)

(local lsp (require :lsp-zero))
(lsp.preset)

(local lspconfig (require :lspconfig))
(lspconfig.lua_ls.setup (lsp.nvim_lua_ls))

(local cmp (require :cmp))
(local cmp_action (lsp.cmp_action))


(lsp.on_attach (fn [client bufnr] lsp.default_keymaps {:buffer bufnr}))

(lspconfig.pylsp.setup {
	:setings {
		:pylsp {
			:plugins {
				:pycodestyle {
					:ignore [:W391 :E303 :E226]
					:maxLineLength 120 }}}}})

(lsp.setup)

(cmp.setup {
	:mapping {
		:<CR> (cmp.mapping.confirm {:select true})

		:<Tab> (cmp.mapping (fn [fallback]
							  (if (cmp.visible)
								(cmp.select_next_item)
								(fallback)) [:i :s] ))

		:<S-Tab> (cmp.mapping (fn [fallback]
								(if (cmp.visible)
									(cmp.select_prev_item)
									(fallback)) [:i :s] ))

		:<C-Space> cmp.mapping.complete

		:<C-f> cmp_action.luasnip_jump_forward
		:<C-b> cmp_action.luasnip_jump_backward }})
