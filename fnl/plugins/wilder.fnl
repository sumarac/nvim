(local wilder (require :wilder))

(wilder.setup {
	:modes [":" "/" "?"]
})

(wilder.set_option :renderer (wilder.renderer_mux {
	":" (wilder.popupmenu_renderer {
		:highlighter wilder.basic_highlighter
		:left [ " " wilder.popupmenu_devicons ]
		:right [ " " wilder.popupmenu_scrollbar ]
	})

	"/" (wilder.popupmenu_renderer {
		:highlighter wilder.basic_highlighter
		:left [ "" wilder.popupmenu_devicons ]
		:right [ "" wilder.popupmenu_scrollbar ]
	})
}))

(wilder.set_option :renderer
	(wilder.popupmenu_renderer
	  { :max_height :20%
		:min_width :100% }))
