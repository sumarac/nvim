(local lazypath (.. (vim.fn.stdpath :data) :/lazy/lazy.nvim))

(if (not (vim.loop.fs_stat lazypath))
  (vim.fn.system
    [:git :clone :--filter=blob:none :https://github.com/folke/lazy.nvim.git :--branch=stable lazypath]))

(vim.opt.rtp:prepend lazypath)

(local lazy (require :lazy))
(local plug 1)
(local opt 1)

(lazy.setup [
	:EdenEast/nightfox.nvim
	:lambdalisue/nerdfont.vim
	:gelguy/wilder.nvim

	:bkad/camelcasemotion
	:lewis6991/gitsigns.nvim

	{
		plug :stevearc/oil.nvim
		:dependencies [
			{plug :nvim-tree/nvim-web-devicons}
		]
	}

	{
		plug :nvim-telescope/telescope.nvim
		:tag "0.1.8"
		:dependencies [
			{plug :nvim-lua/plenary.nvim}
		]
	}

	{
		plug :OXY2DEV/markview.nvim
		:dependencies [
			{plug :nvim-treesitter/nvim-treesitter}
			{plug :nvim-tree/nvim-web-devicons}
		]
	}

	{
		plug :shellRaining/hlchunk.nvim
		:event [
			{opt :UIEnter}
		]
	}

	{
		plug :ThePrimeagen/harpoon
		:branch :harpoon2
		:dependencies [
			{plug :nvim-lua/plenary.nvim}
		]
	}

	{
		plug :nvim-lualine/lualine.nvim
		:dependencies [
			{plug :nvim-tree/nvim-web-devicons}
		]
	}

	{
		plug :nvim-treesitter/nvim-treesitter
		:build ":TSUpdate"
	}

	{
		plug :VonHeikemen/lsp-zero.nvim
		:branch "v3.x"
		:dependencies [
			{plug :neovim/nvim-lspconfig}
			{plug :williamboman/mason.nvim}
			{plug :williamboman/mason-lspconfig.nvim}

			{plug :hrsh7th/nvim-cmp}
			{plug :hrsh7th/cmp-nvim-lsp}
			{plug :L3MON4D3/LuaSnip}
		]
	}

	{
		plug :echasnovski/mini.nvim
		:version false
	}
])


