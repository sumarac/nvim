(local hlchunk (require :hlchunk))

(hlchunk.setup {
	:indent {
		:chars { "." "." "." "." }

		:style {
			:#888888
			:#666666
			:#444444
			:#333333
			:#333333
			:#333333
			:#333333
			:#333333
		}
	}

	:blank {
		:enable false
	}

	:chunk {
		:chars {
			:horizontal_line ""
			:vertical_line ""
			:left_top ""
			:left_bottom ""
			:right_arrow ""
		}

		:style :#FF7F00
	}

	:line_num {
		:style :#FFFFFF
	}

})
