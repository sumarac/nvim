(local dir :options.)
(fn modul [name] (.. dir name))

(require (modul :colors))
(require (modul :keybind))
(require (modul :opts))
