(import-macros {: set!} :hibiscus.vim)

(set! number true)			; enable line number
(set! relativenumber true) 	; enable relative line number
(set! undofile true) 		; persistent undo
(set! backup false) 		; disable backup
(set! autowrite true) 		; auto write buffer when it's not focused
(set! ignorecase true) 		; case insensitive on search..
(set! list true)			; display listchars
(set! smartindent true) 	; smarter indentation
(set! splitright true) 		; split right instead of left
(set! splitkeep :screen) 	; stabilize split
(set! startofline false) 	; don't go to the start of the line when moving to another file
(set! swapfile false) 		; disable swapfile
(set! termguicolors true) 	; true colours for better experience
(set! wrap false)			; don't wrap lines
(set! backupcopy :yes)		; fix weirdness for stuff that replaces the entire file when hot reloading

(set! smarttab false)
(set! tabstop 4)
(set! softtabstop 4)
(set! shiftwidth 4)

(set! compatible false) ; disable compatibility with old vi
(set! showmatch true)	; show matches while searching for text
(set! hlsearch true)	; highlight text that has been searched
(set! incsearch true)	; incramentally search
(set! autoindent true)
(set! wildmode "longest,list")
(set! mouse :v)
(set! mouse :a)
(set! clipboard :unnamedplus) ; use system clipboard
(set! ttyfast true)
(set! cursorline true)
(set! splitbelow true)
(set! autochdir true)
(set! signcolumn :yes:1)
(set! shell :/bin/zsh)
(set! splitright false)
