(require-macros :hibiscus.core)
(require-macros :hibiscus.vim)

(g! mapleader " ")


(map! [n :noremap] :<C-space> "<cmd>Telescope buffers<CR>")

(map! [n :noremap] :<leader>tw "<cmd>lua MiniTrailspace.trim()<CR>")

(map! [n :noremap] :<leader>fo "<cmd>Oil<CR>")
(map! [n :noremap] :<leader>ff "<cmd>Telescope find_files<CR>")
(map! [n :noremap] :<leader>fs "<cmd>Telescope current_buffer_fuzzy_find<CR>")
(map! [n :noremap] :<leader>gs "<cmd>Telescope git_status<CR>")
(map! [n :noremap] :<leader>gg "<cmd>Telescope live_grep<CR>")

; remap keys so that they dont skip camelCase
(map! [n :noremap :silent] :w "<Plug>CamelCaseMotion_w")
(map! [n :noremap :silent] :b "<Plug>CamelCaseMotion_b")
(map! [n :noremap :silent] :e "<Plug>CamelCaseMotion_e")
(map! [n :noremap :silent] :ge "<Plug>CamelCaseMotion_ge")

; this little guy helps you move text, really helpful
(map! [v :noremap] :J ":m '>+1<CR>gv=gv")
(map! [v :noremap] :K ":m '<-2<CR>gv=gv")

; Cursor always stays on center
(map! [n :noremap] :J "mzJ`z")
(map! [n :noremap] :<C-d> "<C-d>zz")
(map! [n :noremap] :<C-u> "<C-u>zz")
(map! [n :noremap] :n "nzzzv")
(map! [n :noremap] :N "Nzzzv")

(map! [x :noremap] :p (fn [] [:_dP]))
(map! [x :noremap] :<leader>p (fn [] [:+dP]))
(map! [n :noremap] :<leader>s ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
(map! [nv :noremap] :<leader>d (fn [] [:_d]))
