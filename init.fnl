;	 ████████╗██████╗ ██╗██╗  ██╗██╗   ██╗
;	 ╚══██╔══╝██╔══██╗██║╚██╗██╔╝╚██╗ ██╔╝
;	    ██║   ██████╔╝██║ ╚███╔╝  ╚████╔╝
;	    ██║   ██╔══██╗██║ ██╔██╗   ╚██╔╝
;	    ██║   ██║  ██║██║██╔╝ ██╗   ██║
;	    ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝   ╚═

;	 Listen up, you motherfuckers,
;	 Those Ivy League dopes, they wanna
;	 mock us. Tell 'em all this is war.
;	 And not fighting a war is for suckers.

(import-macros {: g! : set!} :hibiscus.vim)

; When using neovide, use these settings
(when vim.g.neovide (do
	(set! guifont "FiraCode Nerd Font:h14")
	(g! neovide_scale_factor 1.0)
	(g! neovide_refresh_rate 120)
	(g! neovide_refresh_rate_idle 5)
	(g! neovide_cursor_antialiasing true)
	(g! neovide_cursor_animate_in_insert_mode true)
	(g! neovide_cursor_vfx_mode "ripple")))

; Skip loading of following neovim builtins
(local default_plugins {
	:2html_plugin
	:getscript
	:getscriptPlugin
	:gzip
	:logipat
	:netrw
	:netrwPlugin
	:netrwSettings
	:netrwFileHandlers
	:matchit
	:tar
	:tarPlugin
	:rrhelper
	:spellfile_plugin
	:vimball
	:vimballPlugin
	:zip
	:zipPlugin
	:tutor
	:rplugin
	:syntax
	:synmenu
	:optwin
	:compiler
	:bugreport
	:ftplugin })

(each [id plug (ipairs default_plugins)]
	(g! (.. :loaded plug) 1))


(require :plugins)
(require :options)
